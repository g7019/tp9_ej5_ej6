/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
#include "puertos.h"
#include <stdint.h>
#include <stdio.h>
// Tipos de errores
#define ERROR_MAXMASC "ERROR - La mascara ingresada supera el tamaño del puerto deseado\n" 
#define ERROR_MAX_BIT "ERROR - Ingreso un bit no valido\n"
#define ERROR_MAX_PUERTO "ERROR - el puerto ingresado no es valido\n"

typedef struct // estructura de un numero de 8 bits
{
    uint8_t b0 :1;
    uint8_t b1 :1;
    uint8_t b2 :1;
    uint8_t b3 :1;

    uint8_t b4 :1;
    uint8_t b5 :1;
    uint8_t b6 :1;
    uint8_t b7 :1;
} bits8_t;

typedef struct // estructura de un numero de 16 bits
{
    uint16_t b00 :1;
    uint16_t b01 :1;
    uint16_t b02 :1;
    uint16_t b03 :1;

    uint16_t b04 :1;
    uint16_t b05 :1;
    uint16_t b06 :1;
    uint16_t b07 :1;

    uint16_t b08 :1;
    uint16_t b09 :1;
    uint16_t b10 :1;
    uint16_t b11 :1;

    uint16_t b12 :1;
    uint16_t b13 :1;
    uint16_t b14 :1;
    uint16_t b15 :1;
} bits16_t;

typedef union // estructura de un acumulador A
{
    uint8_t AccA;
    bits8_t Abit;
} AccA_t;

typedef union // estructura de un acumulador B 
{
    uint8_t AccB;
    bits8_t Bbit;
} AccB_t;

typedef struct // Estructura de una union de acumuladores AB
{
    AccB_t B;
    AccA_t A;
} union_t;

typedef union // Estructura de un acumulador D que incluye a los acumuladores A y B
{
    union_t AB;
    uint16_t D;
    bits16_t Dbit;
} AccD_t;


AccD_t port; // Creacion de un puerto del microprocesador


int bitGet(char puerto, int bit)
{
    int value = 0;
    if (puerto == 'A' || puerto == 'a') // si es el puerto A
    {
        uint8_t mask = (DEFAULT_MASK << bit); // se modifica la mascara hasta obtener el bit prendido en el lugar requerido
        if (bit > MAX8_BIT) // si se ingreso una posicion de bit que no existe
        {
            value = -1; // se devuelve el codigo de error -1
        }
        if ( (port.AB.A.AccA & mask) != 0) // si  la comparacion da  distinto de 0 entonces esta prendido
        {
            value = 1;
        }
    }
    else if (puerto == 'B' || puerto == 'b') // si es el puerto B - se procede de la misma forma que el puerto A
    {
        uint8_t mask = (DEFAULT_MASK << bit);
        if (bit > MAX8_BIT)
        {
            value = -1;
        }
        if ( (port.AB.B.AccB & mask) != 0)
        {
            value = 1;
        }
    }
    else if (puerto == 'D' || puerto == 'd') // si es el puerto D
    {
        uint16_t mask = (DEFAULT_MASK << bit); // se procede igual pero con 16 bits
        if (bit > MAX16_BIT)
        {
            value = -1;
        }
        if ( (port.D & mask) != 0)
        {
            value = 1;
        }
    }
    else  // se devuelve el error de puerto no valido
    {
        value = -1; 
    }
    return value;
}

void bitSet(char puerto,int bit)
{
    if (puerto == 'A' || puerto  == 'a') // si es el puerto A
    {
        uint8_t mask = (DEFAULT_MASK << bit); // se modifica la mascara hasta obtener el bit prendido en el lugar requerido
        if (bit <= MAX8_BIT)
        {
            port.AB.A.AccA |= mask; // se hace un or bit a bit para prender el bit deseado y mantener el resto como estaba
        }
        else
        {
            printf(ERROR_MAX_BIT); // se notifica que se ingreso una posicion de bit no existente
        }
    }
    else if (puerto == 'B' || puerto == 'b') // si es el puerto B - se procede igual que en el puerto A
    {
        uint8_t mask = (DEFAULT_MASK << bit);
        if (bit <= MAX8_BIT)
        {
            port.AB.B.AccB |= mask;
        }
        else 
        {
            printf(ERROR_MAX_BIT); 
        }
    }
    else if (puerto == 'D' || puerto == 'd') // si es el puerto D
    {
        uint16_t mask = (DEFAULT_MASK << bit); // es procede de la misma forma que A pero para 16 bits
        if (bit <= MAX16_BIT)
        {
            port.D |= mask;
        }
        else 
        {
            printf(ERROR_MAX_BIT);
        }
    }
    else
    {
        printf(ERROR_MAX_PUERTO); // se notifica si se pidio un puerto no existente
    }
}

void bitClr(char puerto, int bit)
{
    if (puerto == 'A' || puerto == 'a') // si es el puerto A
    {
        uint8_t mask = (~(DEFAULT_MASK << bit)); // se define una mascara con todos los bits encendidos
                                                // excepto por el que se desea apagar
        if (bit <= MAX8_BIT)  // si la posicion ingresadas es valida
        {
            port.AB.A.AccA &= mask; // se hace un and bit a bit para que se mantengan en el mismo estado todos los bits excepto el 
                                    // requerido que se apagara
        }
        else 
        {
            printf(ERROR_MAX_BIT); // se notifica del error
        }
    }
    else if (puerto == 'B' || puerto == 'b') // si es el puerto B - se procede igual que el puerto A
    {
        uint8_t mask = (~(DEFAULT_MASK << bit));
        if (bit <= MAX8_BIT)
        {
            port.AB.B.AccB &= mask;
        }
        else 
        {
            printf(ERROR_MAX_BIT);
        }
    }
    else if (puerto == 'D' || puerto == 'd') // si es el puerto D - se procede igual que el puerto A pero ahora para 16 bits
    {
       uint16_t mask = (~(DEFAULT_MASK << bit));
        if (bit <= MAX16_BIT)
        {
            port.D &= mask;
        }
        else 
        {
            printf(ERROR_MAX_BIT);
        }
    }
    else
    {
        printf(ERROR_MAX_PUERTO); // se notifica si se ingreso un puerto no existente
    }
}

void bitToggle(char puerto, int bit)
{
   if (puerto == 'A' || puerto  == 'a') // si es el puerto A
    {
        uint8_t mask = (DEFAULT_MASK << bit); //  se modifica la mascara hasta obtener el bit prendido en el lugar requerido
        if (bit <= MAX8_BIT) // si la posicion requerida es valida para un numero de 8 bits
        {
            port.AB.A.AccA ^= mask; // se realiza un XOR bit a bit resultando en que el bit prendido en la mascara se cambia de estado
        }
        else
        {
            printf(ERROR_MAX_BIT); // se notifica si se intento a acceder a un bit no valido
        }
    }
    else if (puerto == 'B' || puerto == 'b') // si es el puerto B - se procede igual que en el puerto A
    {
        uint8_t mask = (DEFAULT_MASK << bit);
        if (bit <= MAX8_BIT)
        {
            port.AB.B.AccB ^= mask;
        }
        else 
        {
            printf(ERROR_MAX_BIT);
        }
    }
    else if (puerto == 'D' || puerto == 'd') // si es el puerto D - se procede igual que en el puerto A pero ahora con 16 bit
    {
        uint16_t mask = (DEFAULT_MASK << bit);
        if (bit <= MAX16_BIT)
        {
            port.D ^= mask;
        }
        else 
        {
            printf(ERROR_MAX_BIT);
        }
    }
    else
    {
        printf(ERROR_MAX_PUERTO); // se notifica si el puerto ingresado no existe
    }
}

void maskOn(char puerto, int mascara)
{
    if (puerto == 'A' || puerto  == 'a') // si es el puerto A
    {
        if (mascara < MAXVALUE8BIT) // se chequea que la mascara ingresada sea valida para un numero de 8 bits
        {
            port.AB.A.AccA |= (uint8_t)mascara; // si es asi se hace un or bit a bit con la mascara casteada a un numero de 8 bits
                                                // resultando en que se prenden unicamente los bits prendidos en la mascara
        }
        else 
        {
            printf(ERROR_MAXMASC); // se informa que la mascara no es valida
        }
    }
    else if (puerto == 'B' || puerto == 'b') // si es el puerto B - se procede igual que A
    {
        if (mascara < MAXVALUE8BIT)
        {
            port.AB.B.AccB |= (uint8_t)mascara;
        }
        else 
        {
            printf(ERROR_MAXMASC);
        }
    }
    else if (puerto == 'D' || puerto == 'd') // si es el puerto D - pero ahora con 16 bits
    {
        if (mascara < MAXVALUE16BIT)
        {
            port.D |= (uint16_t)mascara;
        }
        else 
        {
            printf(ERROR_MAXMASC);
        }
    }
    else
    {
        printf(ERROR_MAX_PUERTO); // se notifica que el puerto no existe
    }
}

void maskOff(char puerto, int mascara)
{
    if (puerto == 'A' || puerto  == 'a') // si es el puerto A 
    {
        if (mascara < MAXVALUE8BIT) // se chequea que la mascara ingresada sea valida para un numero de 8 bits
        {
            port.AB.A.AccA &= ~((uint8_t)mascara); // se hace un and bit a bit con la negacion del casteo a 8 bits de la mascara ingresada, apagando todos los 
                                                    // bits encendidos en la mascara ingresada
        } 
        else 
        {
            printf(ERROR_MAXMASC); // se notifica que la mascara es demasiado grande para un numero de 8 bits
        }
    }
    else if (puerto == 'B' || puerto == 'b') // si es el puerto B - se procede igual que en el puerto A 
    {
        if (mascara < MAXVALUE8BIT)
        {
            port.AB.B.AccB &= ~((uint8_t)mascara);
        } 
        else 
        {
            printf(ERROR_MAXMASC);
        }
    }
    else if (puerto == 'D' || puerto == 'd') // si es el puerto D - se procede igual que en el puerto A pero ahora con 16 bits
    {
        if (mascara < MAXVALUE16BIT)
        {
            port.D &= ~((uint16_t)mascara);
        } 
        else 
        {
            printf(ERROR_MAXMASC);
        }
    }
    else
    {
        printf(ERROR_MAX_PUERTO); // se notifica si se ingreso un puerto no existente
    }
}

void maskToggle(char puerto, int mascara)
{
    if (puerto == 'A' || puerto  == 'a') // si es el puerto A
    {
        if (mascara < MAXVALUE8BIT) // se chequea que la mascara ingresada sea valida para un numero de 8 bits
        {
            port.AB.A.AccA ^= (uint8_t)mascara; // se realiza un XOR bit a bit con el casteo a 8 bits de la mascara ingresadad,
                                                // invirtiendo el valor de los bits prendidos en la mascara
        } 
        else 
        {
            printf(ERROR_MAXMASC); // se notifica que la mascara es demasiado grande 
        }
    }
    else if (puerto == 'B' || puerto == 'b') // si es el puerto B - se procede igual que el puerto A 
    {
        if (mascara < MAXVALUE8BIT)
        {
            port.AB.B.AccB ^= (uint8_t)mascara;
        } 
        else 
        {
            printf(ERROR_MAXMASC);
        }
    }
    else if (puerto == 'D' || puerto == 'd') // si es el puerto D - se procede igual que en el puerto A pero con 16 bits
    {
        if (mascara < MAXVALUE16BIT)
        {
            port.D ^= (uint16_t)mascara;
        } 
        else 
        {
            printf(ERROR_MAXMASC);
        }
    }
    else
    {
        printf(ERROR_MAX_PUERTO); // se notifica que es el puerto equivocado
    }
}
