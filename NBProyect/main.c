/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: Grupo 5
 *
 * Created on May 13, 2022, 5:56 PM
 */
#include <stdio.h>
#include <stdint.h>
#include "puertos.h"

#define ESNUM(x) ((x) <= '9' && (x) >= '0')

void print_puerto(void);

int main(void)
{

    printf("-- SIMULACION -- \n"
        "La simulacion del puerto A del HC11 tiene las siguientes acciones:\n"
        "- Ingresando una 't' todos los LEDs se cambian al opuesto a su estado actual\n"
        "- Ingresando una 'c' se apagan todos los LEDs\n"
        "- Ingresando una 's' se prenden todos los LEDs\n"
        "- Ingresando un numero del 1 al 7 se prendera dicho bit\n"
        "Para detener la simulacion debe persionar la letra 'q'\n");

    char ch, puertoA = 'A';
    print_puerto(); // se imprime el puerto A
    while (((ch = getchar()) != 'q' ) && (ch != 'Q') ) // si lo ingresado es distinto de Q
    { // se analiza el dato ingresado
        if (ch == 't') // si es t 
        {
            int mask = 0xFF; // se crea una mascara de con todos los bits en 1
            maskToggle(puertoA,mask); // se invierte el valor de todos los bits del puerto A
            print_puerto(); // se imprime el valor del puerto A
        }
        else if (ch == 's') // si es s
        {
            int mask = 0xFF; // se crea una mascara con todos los bits en 1
            maskOn(puertoA, mask); // se prenden todos los bits del puerto A
            print_puerto(); // se imprime el valor del puerto A
        }
        else if (ch == 'c') // si es c
        {
            int mask = 0xFF; // se crea una mascara con todos los bits en 1
            maskOff(puertoA, mask); // se apagan todos los bits del puerto A
            print_puerto(); // se imprime el puerto A 
        }
        else if (ESNUM(ch)) // si lo ingresado es un numero
        {
            int bit = (ch - '0'); // se convierte el caracter a un valor en int
            bitSet(puertoA, bit); // se prende el bit ingresado 
            print_puerto(); // se imprime el puerto
        }
        else if (ch != '\n') // despreciando el enter 
        {
            printf("El caracter ingresado no es valido\n"); // se notifica al usuario que ingreso un caracter no valido
        }
    }
    return 0;
}

void print_puerto(void)
{
    int counter;
    printf("Puerto A\n"); 
    for (counter = 0; counter <= MAX8_BIT; counter++) // se imprime el puerto A 
    {
        printf("%d", bitGet('A',counter)); // usando la funcion bitGet y aumentando el bit hasta llegar al septimo
    }
    printf("\n");
}