

#ifndef _puertos_h
#define	_puertos_h

#define DEFAULT_MASK 0x01 // crea una mascara con el bit 0 prendido
#define MAX8_BIT 7 // maximo de bits permitidos para un numero de 8 bits
#define MAX16_BIT 15 // maximo de bits permitidos para un numero de 16 bits
#define MAXVALUE8BIT 256 // el siguiente al maximo valor que puede tomar un numero de 8 bits
#define MAXVALUE16BIT 65536 // el siguiente al maximo valor que puede tomar un numero de 16 bits

void bitSet(char puerto, int bit); // dado el puerto y numero de bit cambia su estado a 1
void bitClr(char puerto, int bit); // dado el puerto y numero de bit cambia su estado a 0
int bitGet(char puerto, int bit); // dado el puerto y numero de bit devuelve su valor
void bitToggle(char puerto,int bit); // dado el puerto y numero de bit cambia al estado opuesto en el que esta
void maskOn(char puerto, int bit); // dado un puerto y una mascara cambia a 1 todos los bits prendidos en la mascara
void maskOff(char puerto, int mascara); // dado un puerto y una mascara cambia a 0 todos los bits prendidos en la mascara
void maskToggle(char puerto, int mascara); // dado un puerto y una mascara cambia el estado de todos los bits prendidos en
                                            // la mascara sin cambiar el estado de los restantes



#endif	/* _puertos_h */
