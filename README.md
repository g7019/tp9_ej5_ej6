# TP9_Ej5_Ej6

## Name
TP9 - Simulación de Puerto

## Description
Cuando se realiza software para un microprocesador, no resulta conveniente probarlo
en el mismo para cada modificación que se desea realizar. Es por esto que se suelen
emular los dispositivos y puertos por software para la etapa de debuggeo.
Se pide escribir una librería que permita emular el funcionamiento de los puertos A, B
y D. A y B son dos puertos de 8 bits, configurables tanto de entrada como de salida. D
tiene 16 y es simplemente un alias para los puertos A y B juntos, siendo el B el menos
significativo.
Crear las siguientes funciones o macros: bitSet, bitClr, bitToggle, bitGet,
maskOn, maskOff, maskToggle, utilizables para todos los puertos definidos
anteriormente.
El contenido de los puertos debe almacenarse en una variable estática dentro del
archivo fuente de la librería. El usuario debe poder leer y escribir en los puertos
solamente utilizando las funciones definidas.
Escribir un programa que utilice la librería escrita en el ejercicio 5 para simular que se
tienen 8 LEDs conectados al puerto A. Se debe usar la librería creada en el ejercicio 1,
y mostrar el estado de los LEDs en pantalla.

## Authors and acknowledgment
Miembros del grupo 5:
    Ramiro Belsito 62641
    Agustin Dominguez 62194
    Facundo Caviglia 63178
    Celina Pugliese 63235

